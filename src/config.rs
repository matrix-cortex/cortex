use std::fs::File;
use std::io::Read;

use failure::Error;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct DatabaseConfig {
    pub user: String,
    pub password: String,
    pub database: String,
    pub host: String,
    pub port: u16,
}

#[derive(Debug, Deserialize)]
pub struct SynapseConfig {
    pub server_name: String,
    pub host: String,
    pub port: u16,
    pub presence: bool,
    pub signing_key_path: String,
}

#[derive(Debug, Deserialize)]
pub struct LoggingConfig {
    pub enabled: bool,
    pub level: LoggingLevel,
}

#[derive(Debug, Deserialize)]
pub enum LoggingLevel {
    Debug,
    Info,
    Warn,
    Error,
}

#[derive(Debug, Deserialize)]
pub struct WorkerConfig {
    pub max_concurrency: Option<usize>,
    pub max_hosts_for_presence: Option<usize>,
    pub max_room_cache: Option<usize>,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub version: u8,

    pub name: String,

    pub database: DatabaseConfig,
    pub synapse: SynapseConfig,
    pub worker: WorkerConfig,
    pub logging: LoggingConfig,
}

impl Config {
    pub fn from_file(path: &str) -> Result<Config, Error> {
        let mut file = File::open(path)?;
        let mut data = String::new();
        file.read_to_string(&mut data)?;

        let c: Config = serde_yaml::from_str(&data)?;
        Ok(c)
    }
}
