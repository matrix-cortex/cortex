use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeviceListsOutboundPokes {
    pub destination: String,
    pub stream_id: u64,
    pub user_id: String,
    pub device_id: String,
    pub sent: bool,
    pub ts: u64,
}
