use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DeviceFederationOutbox {
    pub destination: String,
    pub stream_id: u64,
    pub queued_ts: u64,
    pub message_json: serde_json::Value,
}
