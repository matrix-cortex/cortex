use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DeviceContentSet {
    pub deleted: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub device_display_name: Option<String>,
    pub device_id: String,
    pub keys: serde_json::Value,
    pub prev_id: Vec<u64>,
    pub stream_id: u64,
    pub user_id: String,
}
